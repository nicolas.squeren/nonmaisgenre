/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {StyleSheet, useColorScheme, View,} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
//React navigation and stack
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';

// Drawer Navigator and implements all screen access
import { createDrawerNavigator } from '@react-navigation/drawer';
import {DrawerContent} from "./screens/DrawerContent";
import MainTabScreen from "./screens/MainTabScreen";
import SettingsScreen from "./screens/SettingsScreen";
import SupportScreen from "./screens/SupportScreen";
const Drawer = createDrawerNavigator();



const App: () => Node = () => {

  const isDarkMode = useColorScheme() === 'dark';
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
      <NavigationContainer>
        <Drawer.Navigator drawerContent={props => <DrawerContent{...props}/>}>
              <Drawer.Screen name={"HomeDrawer"} component={MainTabScreen}/>
              <Drawer.Screen name={"SupportScreen"} component={SupportScreen}/>
              <Drawer.Screen name={"SettingsScreen"} component={SettingsScreen}/>
        </Drawer.Navigator>
      </NavigationContainer>
  );
};

const styles = StyleSheet.create({
});

export default App;
