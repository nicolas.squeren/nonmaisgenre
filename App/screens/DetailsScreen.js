import React from "react";
import {View, Text, Button, StyleSheet} from 'react-native'

const DetailsScreen = ({navigation}) => {
    return (
        <View style={styles.sectionContainer}>
            <Text>Details SCREEN</Text>
            <Button title={"Click here !"} onPress={ () => alert('Button clicked') } />
            <Button title={"Go to détails screen... again"} onPress={() => navigation.navigate("Details")}/>
            <Button title={"Go to home"} onPress={() => navigation.navigate("Home")}/>
            <Button title={"Go  back"} onPress={() => navigation.goBack()}/>
            <Button title={"Go to the first screen"} onPress={() => navigation.popToTop()}/>
        </View>
    )

}

const styles = StyleSheet.create({
    sectionContainer: {
        flex:1,
        alignItems: 'center',
        marginTop: 32,
        paddingHorizontal: 24,
    },
});

export default DetailsScreen
