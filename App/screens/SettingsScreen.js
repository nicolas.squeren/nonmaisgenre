import React from "react";
import {View, Text, Button, StyleSheet} from 'react-native'

const SettingsScreen = () => {
    return (
        <View style={styles.sectionContainer}>
            <Text>Settings SCREEN</Text>
            <Button title={"Click here !"} onPress={ () => alert('Button clicked') } />
        </View>
    )

}

const styles = StyleSheet.create({
    sectionContainer: {
        flex:1,
        alignItems: 'center',
        marginTop: 32,
        paddingHorizontal: 24,
    },
});

export default SettingsScreen
