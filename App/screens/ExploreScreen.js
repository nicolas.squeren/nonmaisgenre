import React from "react";
import {View, Text, StyleSheet} from 'react-native'
import {Button} from "react-native-paper";


const ExploreScreen = ({navigation}) => {
    return (
        <View style={styles.sectionContainer}>
            <Text>ExploreScreen SCREEN</Text>
            <Button onPress={() => navigation.navigate("Details")}>Go to détails screen -></Button>
        </View>
    )

}


const styles = StyleSheet.create({
    sectionContainer: {
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
});

export default ExploreScreen
