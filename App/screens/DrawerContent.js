import React, {useState} from "react";
import {View, StyleSheet} from 'react-native';
import {
    Avatar,
    Text,
    Title,
    Caption,
    Paragraph,
    Drawer,
    TouchableRipple,
    Switch
} from 'react-native-paper';
import {
    DrawerContentScrollView,
    DrawerItem
} from '@react-navigation/drawer';
import Icon from "react-native-vector-icons/FontAwesome5";

export function DrawerContent(props) {

    const [isDarkTheme, setIsDarkTheme] = useState(false);

    const toggleTheme = () => {
        setIsDarkTheme(!isDarkTheme);
    }

    return (
        <View style={{flex:1}}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                    <View style={styles.userInfoSection}>
                        <View style={{flexDirection:'row', marginTop:15}}>
                            <Avatar.Image
                                source={{
                                    uri:'https://e7.pngegg.com/pngimages/799/987/png-clipart-computer-icons-avatar-icon-design-avatar-heroes-computer-wallpaper.png'
                                }}
                                size={50}
                            />
                            <View style={{flexDirection:'column', marginLeft:15}}>
                                <Title style={styles.title}>Nicolas SQUEREN</Title>
                                <Caption style={styles.caption}>@nico44</Caption>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>80</Paragraph>
                                <Caption style={styles.caption}>Following</Caption>
                            </View>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>100</Paragraph>
                                <Caption style={styles.caption}>Followers</Caption>
                            </View>
                        </View>
                    </View>
                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem
                            icon={(color, size) =>(
                                <Icon
                                    name={"home"}
                                    color={color}
                                    size={size}
                                />
                            )}
                            label={"Home"}
                            onPress={ () => {props.navigation.navigate('Home')} }
                        />
                        <DrawerItem
                            icon={(color, size) =>(
                                <Icon
                                    name={"user"}
                                    color={color}
                                    size={size}
                                />
                            )}
                            label={"Détails"}
                            onPress={ () => {props.navigation.navigate('Details')} }
                        />
                        <DrawerItem
                            icon={(color, size) =>(
                                <Icon
                                    name={"user"}
                                    color={color}
                                    size={size}
                                />
                            )}
                            label={"Profile"}
                            onPress={ () => {props.navigation.navigate('Profile')} }
                        />
                        <DrawerItem
                            icon={(color, size) =>(
                                <Icon
                                    name={"cogs"}
                                    color={color}
                                    size={size}
                                />
                            )}
                            label={"Settings"}
                            onPress={ () => {props.navigation.navigate('SettingsScreen')} }
                        />
                        <DrawerItem
                            icon={(color, size) =>(
                                <Icon
                                    name={"question"}
                                    color={color}

                                    size={size}
                                />
                            )}
                            label={"Support"}
                            onPress={ () => {props.navigation.navigate('SupportScreen')} }
                        />
                    </Drawer.Section>
                    <Drawer.Section title={"Preferences"}>
                        <TouchableRipple onPress={() => {toggleTheme()} }>
                            <View style={styles.prefrence}>
                                <Text>dark theme</Text>
                                <View pointerEvents={"none"}>
                                    <Switch value={isDarkTheme}/>
                                </View>
                            </View>
                        </TouchableRipple>

                    </Drawer.Section>
                </View>
            </DrawerContentScrollView>
            <Drawer.Section style={styles.bottomDrawerSection}>
                <DrawerItem
                    icon={(color, size) =>(
                        <Icon
                            name={"sign-out-alt"}
                            color={color}
                            size={size}
                        />
                    )}
                    label={"Sign Out"}
                    onPress={ () => {} }
                />
                <DrawerItem
                    icon={(color, size) =>(
                        <Icon
                            name={"user-cog"}
                            color={color}
                            size={size}
                        />
                    )}
                    label={"Login"}
                    onPress={ () => {props.navigation.navigate('Login')} }
                />
            </Drawer.Section>
        </View>
    )

}

const styles = StyleSheet.create({
    drawerContent:{
        flex:1,
    },
    userInfoSection:{
        paddingLeft: 20,
    },
    title:{
        fontSize:16,
        marginTop:3,
        fontWeight: 'bold',
    },
    caption:{
        fontSize: 14,
        lineHeight:14,
    },
    row:{
        marginTop: 20,
        flexDirection:'row',
        alignItems:'center',
    },
    section:{
        flexDirection: 'row',
        alignItems: 'center',
        marginRight:15,
    },
    paragraph:{
        fontWeight: 'bold',
        marginRight: 3,
    },
    drawerSection:{
        marginTop:15,
    },
    bottomDrawerSection:{
        marginBottom:15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    prefrence:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical:12,
        paddingHorizontal: 16,
    },
})
