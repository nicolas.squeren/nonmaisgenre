import React from "react";
import {View, Text, StyleSheet} from 'react-native'
import {IconButton} from "react-native-paper";
import {createStackNavigator} from "@react-navigation/stack";
import DetailsScreen from "../screens/DetailsScreen";
import HomeScreen from "../screens/HomeScreen";

import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import ExploreScreen from "./ExploreScreen";
import ProfileScreen from "./ProfileScreen";

const Tab = createMaterialBottomTabNavigator();

//Create Stack navigator constant
const HomeStack = createStackNavigator();
const DetailsStack = createStackNavigator();


const HomeStackScreen = ({navigation}) => (
    // Create stack navigator, screenoptions for all stackscreen
    <HomeStack.Navigator screenOptions={{
        headerStyle:{
            backgroundColor: '#B8B3E9',
        },
        headerTintColor:'#fff',
        headerTitleStyle:{
            fontWeight:'bold',
            justifyContent:'center'
        }
    }}>
        {/*options for one elements stackscreen*/}
        <HomeStack.Screen name={"Home"} component={HomeScreen} options={{
            headerStyle:{
                backgroundColor: '#3A506B',
            },
            headerTintColor:'#fff',
            headerTitleStyle:{
                fontWeight:'bold',
                justifyContent:'center'
            },
            headerLeft:() => (
                <IconButton icon="menu" size={25} color={"#fff"} onPress={() => {
    navigation.openDrawer()
}}/>
            )
        }}/>
    </HomeStack.Navigator>
);

const DetailsStackScreen = ({navigation}) => (
    // Create stack navigator, screenoptions for all stackscreen
    <DetailsStack.Navigator screenOptions={{
        headerStyle:{
            backgroundColor: '#B8B3E9',
        },
        headerTintColor:'#fff',
        headerTitleStyle:{
            fontWeight:'bold',
            justifyContent:'center'
        },
        headerLeft:() => (
            <IconButton icon="menu" size={25} color={"#fff"} onPress={() => {
    navigation.openDrawer()
}}/>
        )
    }}>
        <DetailsStack.Screen name={"Details"} component={DetailsScreen}/>
    </DetailsStack.Navigator>
);

const MainTabScreen = ({navigation}) => {
    return (
        <Tab.Navigator
            initialRouteName="Home"
            activeColor="#fff"
            barStyle={{ backgroundColor: '#3A506B' }}
        >
            <Tab.Screen
                name="Home"
                component={HomeStackScreen}
                options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="home" color={color} size={26} />
                    ),
                }}
            />
            <Tab.Screen
                name="Details"
                component={DetailsStackScreen}
                options={{
                    tabBarLabel: 'Details',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="bell" color={color} size={26} />
                    ),
                }}
            />
            <Tab.Screen
                name="Profile"
                component={ProfileScreen}
                options={{
                    tabBarLabel: 'Profile',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="account" color={color} size={26} />
                    ),
                }}
            />
            <Tab.Screen
                name="Explore"
                component={ExploreScreen}
                options={{
                    tabBarLabel: 'Help',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="help" color={color} size={26} />
                    ),
                }}
            />
        </Tab.Navigator>

    )

}


const styles = StyleSheet.create({
    sectionContainer: {
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
});

export default MainTabScreen
